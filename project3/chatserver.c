#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <dirent.h>
#include <sys/stat.h>
#include <pthread.h>
#include "pg3lib.h"

void *handle_client(void *);

typedef struct {
	int sock;
	int message_sock;
} 

socks;

void *handle_client(void *s) {
	
	char *char_yes = "yes";
	char *char_no = "no";
	
	
    int STATUS = 0;
    // socket number
	int sock = ((socks *)s)->sock;
	int message_sock = ((socks *)s)->message_sock;
	
	// open users file
	FILE *usersfile = fopen("users.txt", "a+");
	
	
	char username[512];
    // get username
	memset(username, 0, sizeof(username));
	if (recv(sock, (char *)username, sizeof(username), 0) < 0) {
		fprintf(stderr, "Server can not recv username: %s\n", strerror(errno));
		STATUS = 1;
		goto cleanup;
	}
	
	// Check for username in file
	bool user_exists = 0;

	char uBUF[512];
	char pBUF[512];
	char *user = uBUF;
	char *pswd = pBUF;

	char line[512];
	char *line_str = line;

	size_t line_buf_size = 512;

	memset(line, 0, sizeof(line));
	memset(uBUF, 0, sizeof(uBUF));
	memset(pBUF, 0, sizeof(pBUF));

	while (getline(&line_str, &line_buf_size, usersfile)) {

		user = strtok(line, " ");
		pswd = strtok(NULL, "\n");
		if(user == NULL){
			break;
		}
		if (strcmp(user, username) == 0) {
			user_exists = 1;
			break;
		}
		memset(line, 0, sizeof(line));
		memset(uBUF, 0, sizeof(uBUF));
		memset(pBUF, 0, sizeof(pBUF));
	}
	if (user_exists) {
		char *resp = "yes";
		if (send(sock, resp, strlen(resp), 0) < 0) {
			fprintf(stderr, "Server can not acknowledge user was found: %s\n", strerror(errno));
			STATUS = 1;
			goto cleanup;
		}
	}
	else {
		char *resp = "no";
		if (send(sock, resp, strlen(resp), 0) < 0) {
			fprintf(stderr, "Error: Server can't acknowledge user was not found: %s\n", strerror(errno));
			STATUS = 1;
			goto cleanup;
		}
	}
	
	// Receive password
	char password[512];
	memset(password, 0, sizeof(password));
	if (recv(sock, (char *)password, sizeof(password), 0) < 0) {
		fprintf(stderr, "Error: failed to get password from client: %s\n", strerror(errno));
		STATUS = 1;
		goto cleanup;
	}
	
	//  check password if user exists, if not add the user to file.
	if (user_exists) {
		// Check if password is right, if not, keep recving password until it is
		while (strcmp(password, pswd) != 0) {
			memset(password, 0, sizeof(password));
			if (send(sock, char_no, strlen(char_no), 0) < 0) {	
				fprintf(stderr, "Error: Server can't reject password: %s\n", strerror(errno));
				STATUS = 1;
				goto cleanup;
			}
			if (recv(sock, (char *)password, sizeof(password), 0) < 0) {
				fprintf(stderr, "Error: Server can't recv new password attempt: %s\n", strerror(errno));
				STATUS = 1;
				goto cleanup;
			}
		}
		// Now that we have the right password, ack yes
		if (send(sock, char_yes, strlen(char_yes), 0) < 0) {
			fprintf(stderr, "Error: Server can't accept password: %s\n", strerror(errno));
			STATUS = 1;
			goto cleanup;
		}
	}
	else {
		// Build string and append that combo
		char append_line[1024];
		memset(append_line, 0, sizeof(append_line));
		if (sprintf((char *)append_line, "%s %s\n", username, password) < 0) {
			fprintf(stderr, "Error: can't build \"user pswd\" string: %s\n", strerror(errno));
			STATUS = 1;
			goto cleanup;
		}
		if (fwrite(append_line, strlen(append_line), 1, usersfile) < 0) {
			fprintf(stderr, "Error: can't append user/pswd combo to file: %s\n", strerror(errno));
			STATUS = 1;
			goto cleanup;
		}
		// Ack "yes"
		if (send(sock, char_yes, strlen(char_yes), 0) < 0) {
			fprintf(stderr, "Error: Server can't acknowledge that user was added: %s\n", strerror(errno));
			STATUS = 1;
			goto cleanup;
		}
		
	}
	
	
	// Encryption steps

	char *pubkey = getPubKey();
	char client_key[4096];

	memset(client_key, 0, sizeof(client_key));

	if (recv(sock, (char *)client_key, sizeof(client_key), 0) < 0) {

		fprintf(stderr, "Erro: can't recv client key: %s\n", strerror(errno));
		STATUS = 1;
		goto cleanup;
	}
	
	char *encryptedkey = encrypt(pubkey, client_key);

	if (send(sock, encryptedkey, strlen(encryptedkey), 0) < 0) {
		fprintf(stderr, "Error: can't send encrypted key back to client: %s\n", strerror(errno));
		STATUS = 1;
		goto cleanup;
	}
	
	FILE *active_users = fopen("activeusers.txt", "a");
	
	char add_active[8192];

	sprintf((char *)add_active, "%s:%d:%d:%s\n", username, sock, message_sock, client_key);

	if (fwrite(add_active, strlen(add_active), 1, active_users) < 0) {

		fprintf(stderr, "Error: can't append active user to file: %s\n", strerror(errno));
		STATUS = 1;
		fclose(active_users);
		goto cleanup;
	}
	
	// close file
	fclose(active_users);
	
	printf("Connection Accepted.\n");	
	while (1) {
		char command[10];
		char message[4096];
		char users[4096];
		char selected_user[512];
		char receiverPubKey[4096];

		memset(receiverPubKey, 0, sizeof(receiverPubKey));
		memset(selected_user, 0, sizeof(selected_user));
		memset(users, 0, sizeof(users));
		memset(message, 0, sizeof(message));
		memset(command, 0, sizeof(command));
		
		char *inv_string = "inv";
		// Receive the command from the client
		if (recv(sock, (char *)command, sizeof(command), 0) < 0) {
			fprintf(stderr, "Error: can't receive command from the client: %s\n", strerror(errno));
			STATUS = 1;
			goto cleanup;
		}
		// Send ack that command was received
		if (send(sock, char_yes, strlen(char_yes), 0) < 0) {
			fprintf(stderr, "Error: can't send ack that command was recvd: %s\n", strerror(errno));
			STATUS = 1;
			goto cleanup;
		}
        printf("The current command value: %s\n", command);
		// seperate into three different categories depending on case scenario
		if (strcmp(command, "P") == 0) {
			
			// Receive message from user
			if (recv(sock, (char *)message, sizeof(message), 0) < 0) {
				fprintf(stderr, "Error: can't get message from user: %s\n", strerror(errno));
				STATUS = 1;
				goto cleanup;
			}
		    printf("PUBLIC MESSAGE\n");
            printf("message recieved: %s\n", message);
			// Send ack "yes" that message was received
			if (send(sock, char_yes, strlen(char_yes), 0) < 0) {
				fprintf(stderr, "Error: can't send ack that message was received: %s\n", strerror(errno));
				STATUS = 1;
				goto cleanup;
            }
			
			// Open file descriptor for reading the active users file
			active_users = fopen("activeusers.txt", "r");

			if(active_users == NULL){
				fprintf(stderr, "Error: can't open activeusers.txt: %s\n", strerror(errno));
			}

			size_t len = 0;

			char *line = NULL;
			char *pubUsername = NULL;
			char *pubSock = NULL;
			char lineBUF[1024];
			char pubTemp[1024];
			char prefix [1024];

			memset(pubTemp, 0, sizeof(pubTemp));
			memset(prefix, 0, sizeof(prefix));
			memset(lineBUF, 0, sizeof(lineBUF));
			
			// Read and send to each of the active users
			while(getline(&line, &len, active_users) != -1){
				strcpy(lineBUF, line);
				pubUsername = strtok(lineBUF, ":");
				char *firstsock = strtok(NULL, ":");
				pubSock = strtok(NULL, ":");
				sprintf(prefix, "P: %s", username);
				if(strcmp(pubUsername, username) != 0){
					sprintf(pubTemp, "%s:%s", prefix, message);
                    if(send(atoi(pubSock), pubTemp, strlen(pubTemp), 0) < 0){
						fprintf(stderr, "can't send ack that P message was received: %s\n", strerror(errno));
						STATUS = 1;
						fclose(active_users);
						goto cleanup;
					}	
				}
				memset(pubTemp, 0, sizeof(pubTemp));
				memset(prefix, 0, sizeof(prefix));
				memset(lineBUF, 0, sizeof(lineBUF));
				memset(pubSock, 0, sizeof(pubSock));
				memset(pubUsername, 0, sizeof(pubUsername));
			}
			// Close file descriptor (will open again later for reading/writing, not appending)
			fclose(active_users);
	
		}
		// D: Direct message
		else if (strcmp(command, "D") == 0) {
			// Loop through users in activeusers file and format
			// the string users as "  user1\n  user2\n..."
		    printf("PRIVATE MESSAGE\n");
			active_users = fopen("activeusers.txt", "r");
			char userline[8192];
			char *userline_string = userline;
			size_t userline_size = 8192;
			memset(userline, 0, sizeof(userline));
			char *userlist_ptr = users;
			
			while (getline(&userline_string, &userline_size, active_users) > 0) {
				char *curr_user = strtok(userline_string, ":");
				if (strcmp(curr_user, username) == 0) continue;
				sprintf(userlist_ptr, "  %s\n", curr_user);
				userlist_ptr += (3 + strlen(curr_user));
				memset(userline, 0, sizeof(userline));
			}
			
			// Close active users file
			fclose(active_users);
			// Send users to client\

			if (strlen(users) == 0) {
				sprintf((char *)users, "X");
			}
			if (send(sock, (char *)users, strlen(users), 0) < 0) {
				fprintf(stderr, "can't send user list to client: %s\n", strerror(errno));
				STATUS = 1;
				goto cleanup;
			}
			if (strcmp(users, "X") == 0) continue;
			// Receive username that client would like to talk to
			if (recv(sock, (char *)selected_user, sizeof(selected_user), 0) < 0) {
				fprintf(stderr, "can't receive selected user from client: %s\n", strerror(errno));
				STATUS = 1;
				goto cleanup;
			}
			
			bool no_user = 1;
			int receiver_sock;
			
			active_users = fopen("activeusers.txt", "r");
			
			memset(userline, 0, sizeof(userline));
			while (getline(&userline_string, &userline_size, active_users)) {
				char *curr_user = strtok(userline_string, ":");
				if (curr_user == NULL) {
					break;
				}
				char *first_sock = strtok(NULL, ":");
				char *curr_sock = strtok(NULL, ":");
				char *curr_key = strtok(NULL, "\n");
				if (curr_sock) receiver_sock = atoi(curr_sock);
				if (curr_key) sprintf(receiverPubKey, "%s", curr_key);
				if (curr_user && strcmp(curr_user, selected_user) == 0) {
					no_user = 0;
					break;
				}
				memset(userline, 0, sizeof(userline));
			}
			
			fclose(active_users);
			
			
			// Send receiverPubKey to the client
			if (strlen(receiverPubKey) < 10) {
				sprintf(receiverPubKey, "%s", pubkey);
			}
			if (send(sock, (char *)receiverPubKey, strlen(receiverPubKey), 0) < 0) {
				fprintf(stderr, "Error: can't send receiver's public key to client: %s\n", strerror(errno));
				STATUS = 1;
				goto cleanup;
			}
			
			// Receive message from the user
			if (recv(sock, (char *)message, sizeof(message), 0) < 0) {
				fprintf(stderr, "Error: can't recv D message from client: %s\n", strerror(errno));
				STATUS = 1;
				goto cleanup;
			}
			
			// If no_user, continue else check if the user is active			
			if (no_user) {
                // If user is still active, send yes, else send invalid
				if (send(sock, inv_string, strlen(inv_string), 0) < 0) {
					fprintf(stderr, "can't tell client that user is inv: %s\n", strerror(errno));
					STATUS = 1;
					goto cleanup;
				}
				continue;
			}
			else {
				// Double check
				active_users = fopen("activeusers.txt", "r");
				bool still_active = 0;
				memset(userline, 0, sizeof(userline));

				while (getline(&userline_string, &userline_size, active_users)) {

					char *curr_user = strtok(userline_string, ":");

					if (curr_user == NULL) {
						break;
					}
					if (curr_user && strcmp(curr_user, selected_user) == 0) {
						still_active = 1;
						break;
					}
					memset(userline, 0, sizeof(userline));
				}

				fclose(active_users);

				if (still_active) {

					if (send(sock, char_yes, strlen(char_yes), 0) < 0) {
						fprintf(stderr, "Error: can't tell client that user is online: %s\n", strerror(errno));
						STATUS = 1;
						goto cleanup;
					}
				}
				else {

					if (send(sock, inv_string, strlen(inv_string), 0) < 0) {
						fprintf(stderr, "Error: can't tell client that user is inv (2): %s\n", strerror(errno));
						STATUS = 1;
						goto cleanup;
					}
					continue;
				}
			}
			
			// build and send message to active user
			char built_message[4096];
			memset(built_message, 0, sizeof(built_message));

			sprintf((char *)built_message, "D:%s:%s\n", username, message);

			if (send(receiver_sock, (char *)built_message, strlen(built_message), 0) < 0) {

				fprintf(stderr, "Error: can't send D message to other user: %s\n", strerror(errno));
				STATUS = 1;
				goto cleanup;
			}
		}
		// end if user presses q
		else if (strcmp(command, "Q") == 0) {
			goto cleanup;
		}
		else {

			fprintf(stderr, "Error: Invalid command received from user: not P, D, or Q.\n");
			STATUS = 1;
			goto cleanup;
		}
	}
	
	

cleanup: ;

	char ln[4096];		
	char* usr;

	FILE *tmpactives = fopen("tmp.txt", "w");
	active_users = fopen("activeusers.txt", "r");

	char temp[4096];			

	while (fgets(ln,4096,active_users)) {
		strcpy(temp, ln);
		usr = strtok(ln,":");

		if (strcmp((char*)usr,username) != 0) {
			fputs(temp,tmpactives);
		}
	}

	fclose(active_users);

	remove("activeusers.txt");
	rename("tmp.txt","activeusers.txt");
    printf("EXIT");
	fclose(tmpactives);
	fclose(usersfile);

	close(sock);
	close(message_sock);

	pthread_exit(NULL);

	exit(STATUS);
}

int main(int argc, char *argv[]) {

	if (argc != 2) {
		fprintf(stderr, "Usage: %s <port>\n", argv[0]);
		return 1;
	}
	
	int SERVER_PORT = atoi(argv[1]);			
	
	struct sockaddr_in server_address;
	memset(&server_address, 0, sizeof(server_address));

	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(SERVER_PORT);
	server_address.sin_addr.s_addr = htonl(INADDR_ANY);
	
	// create socket
	int listen_sock;
	if ((listen_sock = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		fprintf(stderr, "Error: can't make listen sock: %s\n", strerror(errno));
		return 1;
	}
	
	// cind socket
	if ((bind(listen_sock, (struct sockaddr *)&server_address, sizeof(server_address))) < 0) {
		fprintf(stderr, "Error: can't bind server sock: %s\n", strerror(errno));
		return 1;
	}
	
	// listen
	int wait_size = 16;
	if (listen(listen_sock, wait_size) < 0) {
		fprintf(stderr, "Error: can't open socket: %s\n", strerror(errno));
		return 1;
	}
	
	// accept any connections
	struct sockaddr_in client_address;
	int client_address_len = 0;
	printf("Accepting connections on port %d\n", SERVER_PORT);
	
	pthread_t threads[512]; 
	
	// loop and accept connections
	while (1) {

		int sock;
		if ((sock = accept(listen_sock, (struct sockaddr *)&client_address, &client_address_len)) < 0) {
			fprintf(stderr, "Error: can't open socket for a client: %s\n", strerror(errno));
			return 1;
		}
		
		int message_sock;
		if ((message_sock = accept(listen_sock, (struct sockaddr *)&client_address, &client_address_len)) < 0) {
			fprintf(stderr, "Error: can't make message socket client: %s\n", strerror(errno));
			return 1;
		}
		
		socks s = {sock, message_sock};
		
		// create thread for client and continue listening
		int rc = pthread_create(&threads[sock-3], NULL, handle_client, (void *)&s); 
		
		if (rc != 0) {
			fprintf(stderr, "Error: can't create thread: %s\n", strerror(errno));
			return 1;
		}
	}
	
	return 0;
}




