#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include "pg3lib.h"

void * handleMessages(void * val);
int main(int argc, char *argv[])
{
    if (argc != 4)
    {
        fprintf(stderr, "Usage: %s [Server Name] [Port] [Username]\n", argv[0]);
        return 1;
    }

    char * serverName = argv[1];
    int serverPort = atoi(argv[2]);
    char * userName   = argv[3];

    struct sockaddr_in serverAddress;
    memset(&serverAddress, 0, sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;

    struct hostent *hp;
    hp = (struct hostent *)gethostbyname(serverName);
    bcopy(hp->h_addr, (char *)&serverAddress.sin_addr, hp->h_length);

    inet_pton(AF_INET, serverName, &serverAddress.sin_addr);
    serverAddress.sin_port = htons(serverPort);

    int socketVal;
    if((socketVal = socket(PF_INET, SOCK_STREAM, 0)) < 0)
    {
        fprintf(stderr, "Unable to create client socket: %s\n", strerror(errno));
        return 1;
    }

    if (connect(socketVal, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0)
    {
        fprintf(stderr, "Unable to connect to the server: %s\n", strerror(errno));
        close(socketVal);
        return 1;

    }

    int messageVal;
    if((messageVal = socket(PF_INET, SOCK_STREAM, 0)) < 0)
    {
        fprintf(stderr, "Unable to create client socket: %s\n", strerror(errno));
        return 1;
    }

    if (connect(messageVal, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0)
    {
        fprintf(stderr, "Unable to connect to the server: %s\n", strerror(errno));
        close(messageVal);
        return 1;

    }

    printf("Connecting to %s on port %d\n", serverName, serverPort);

    if (send(socketVal, userName, strlen(userName), 0) < 0) 
    {
        fprintf(stderr, "Unable to send username: %s\n", strerror(errno));
        close(socketVal);
        return 1;
    }

    char eBuf[10];

    if(recv(socketVal, (char *)eBuf, sizeof(eBuf), 0) < 0)
    {
        fprintf(stderr, "Unable to check if user exists: %s\n", strerror(errno));
        close(socketVal);
        return 1;
    }



    char * yesValue = "yes";
    char * noValue = "no";
    if(strcmp(eBuf, noValue) == 0)
    {
        printf("Creating New User\nEnter Password: ");
    }else if (strcmp(eBuf, yesValue) == 0)
    {
        printf("Enter Password: ");
    }else{
        fprintf(stderr, "Unable to acknowledge user: %s\n", eBuf);
        close(socketVal);
        return 1;
    }

    char password[512];
    fgets((char *)password, sizeof(password), stdin);
    while(strlen(password) < 1)
    {
        fgets((char *)password, sizeof(password), stdin);
    }
    password[strlen(password) - 1] = '\0';

    if (send(socketVal, password, strlen(password), 0) < 0)
    {
        fprintf(stderr, "Unable to send password: %s\n", strerror(errno));
        close(socketVal);
        return 1;
    }

    char actualPassword[10];
    memset(actualPassword, 0, sizeof(actualPassword));
    if (recv(socketVal, (char *)actualPassword, sizeof(actualPassword), 0) <0)
    {
        fprintf(stderr, "Unable to acknowledge password: %s\n", strerror(errno));
        close(socketVal);
        return 1;
    }

    while (strcmp(actualPassword, noValue) == 0)
    {
        printf("Incorrect User Password. Re-Enter: ");
        memset(password, 0, sizeof(password));
        fgets((char *)password, sizeof(password), stdin);
        while(strlen(password) < 1)
        {
            fgets((char *)password, sizeof(password), stdin);
        }
        password[strlen(password) - 1] = '\0';
        if (send(socketVal, password, strlen(password), 0) < 0)
        {
            fprintf(stderr, "Unable to send password: %s\n", strerror(errno));
            close(socketVal);
            return 1;
        }
        memset(actualPassword, 0, sizeof(actualPassword));
        if (recv(socketVal, (char *)actualPassword, sizeof(actualPassword), 0) <0)
        {
            fprintf(stderr, "Unable to acknowledge password: %s\n", strerror(errno));
            close(socketVal);
            return 1;
        }
    }

    if(strcmp(actualPassword, yesValue) != 0)
    {
        fprintf(stderr, "Unable to recieve password acknowledgements: %s\n", actualPassword);
        close(socketVal);
        return 1;
    }
    
    /* Create name text file */
    char textFileName[100];
    memset(textFileName, 0, sizeof(textFileName));
    sprintf(textFileName, "%s.txt", userName);
    printf("USERNAME: %s\n", textFileName);
    FILE * nameFile = fopen(textFileName, "a+");

    
    char *publicKey = getPubKey();
    int kLength = strlen(publicKey);
    if (send(socketVal, publicKey, kLength, 0) < 0)
    {
        fprintf(stderr, "Unable to send public key: %s\n", strerror(errno));
        close(socketVal);
        return 1;
    }

    char protectedKey[4096];
    memset(protectedKey, 0, sizeof(protectedKey));
    if (recv(socketVal, (char *)protectedKey, sizeof(protectedKey), 0) < 0)
    {
        fprintf(stderr, "Unable to recieve host's public key: %s\n", strerror(errno));
        close(socketVal);
        return 1;
    }

    char * hostKey = decrypt(protectedKey);

    pthread_t pt;

    int responseVal = pthread_create(&pt, NULL, handleMessages, (void *)&messageVal);
    if (responseVal != 0)
    {
        fprintf(stderr, "Unable to create thread: %s\n", strerror(errno));
        return 1;
    }

    while (1)
    {
        char ackno[10];
        char allUsers[4096];
        char buffer[10];
        char confirmation[10];
        char encMessageBuffer[4096];
        char  *encMessage = encMessageBuffer;
        char *invalidUser = "inv";
        char message[4096];
        char rPublicKey[4096];
        char username[512];

        memset(ackno,  0, sizeof(ackno));
        memset(allUsers, 0, sizeof(allUsers));
        memset(buffer, 0, sizeof(buffer));
        memset(confirmation, 0, sizeof(confirmation));      
        memset(encMessageBuffer, 0, sizeof(encMessageBuffer));
        memset(message, 0, sizeof(message));
        memset(rPublicKey, 0, sizeof(rPublicKey));
        memset(username, 0, sizeof(username));
        
        printf("Enter Command: (P) for public messages, (D) for direct messages, (H) for history, or (Q) to quit.\n");
        printf(">>> ");
        fgets((char *)buffer, sizeof(buffer), stdin);
        buffer[strlen(buffer) -  1] = '\0';

        while(strcmp((char *)buffer, "P") != 0 && strcmp((char *)buffer, "D") != 0 && strcmp((char *)buffer, "H") != 0 && strcmp((char *)buffer, "Q") != 0)
        {
            memset(buffer, 0, sizeof(buffer));
            printf("Unable to analyze response. (P) (D) (H) or (Q)\n");
            printf(">>> ");
            fgets((char *)buffer, sizeof(buffer), stdin);
            buffer[strlen(buffer) - 1] = '\0';
        }

        if (send(socketVal, (char *)buffer, strlen(buffer), 0) < 0)
        {
            fprintf(stderr, "Unable to send user command to server: %s\n", strerror(errno));
            return 1;
        }

        if (recv(socketVal, (char *)ackno, sizeof(ackno), 0) < 0)
        {
            fprintf(stderr, "Unable to recieve acknowledgement: %s\n", strerror(errno));
            return 1;
        }
        
        if (strcmp(ackno, yesValue) != 0)
        {
            fprintf(stderr, "Unable to recognize  acknowledgement: %s\n", strerror(errno));
            return 1;
        }
        
        if (strcmp(buffer, "P") == 0)
        {
            printf("Enter User Message\n>>> ");
            fgets((char *)message, sizeof(message), stdin);
            message[strlen(message) - 1] = '\0';

            if (send(socketVal, (char *)message, strlen(message), 0) < 0)
            {
                fprintf(stderr, "Unable to send message to the server: %s\n", strerror(errno));
                return 1;
            }

            if (recv( socketVal, (char *)confirmation, sizeof(confirmation), 0) < 0)
            {
                fprintf(stderr, "Unable to recieve confirmation: %s\n", strerror(errno));
                return 1;
            }

            if (strcmp(confirmation, yesValue) != 0)
            {
                fprintf(stderr, "Unable to recognize confirmation: %s\n");
                return 1;
            }
        } else if ( strcmp(buffer, "D") == 0)
        {
            if (recv(socketVal, (char *)allUsers, sizeof(allUsers), 0) < 0)
            {
                fprintf(stderr, "Unable to recieve list of all users: %s\n", strerror(errno));
                return 1;
            }
            if (strcmp(allUsers, "X") == 0)
            {
                printf("No available users online. \n");
                continue;
            }
            printf("All Users Online: \n%s\n", allUsers);
            printf("Enter User that you would like to talk to\n>>> ");

            fgets((char *)username, sizeof(username), stdin);
            username[strlen(username)-1] = '\0';

            if (send(socketVal, (char *)username, strlen(username), 0) < 0)
            {
                fprintf(stderr, "Unable to send username to server: %s\n", strerror(errno));
                return 1;
            }

            if (recv(socketVal, (char *)rPublicKey, sizeof(rPublicKey), 0) < 0)
            {
                fprintf(stderr, "Unable to recieve public key of users from server: %s\n", strerror(errno));
                return 1;
            }

            printf("Enter message\n>>> ");
            fgets((char *)message, sizeof(message), stdin);
            message[strlen(message) - 1] = '\0';

            // encMessage = encrypt((char *)message, rPublicKey);
            if (send(socketVal, (char *)message, strlen(message), 0) < 0)
            {
                fprintf(stderr, "Unable to send message to server: %s\n", strerror(errno));
                return 1;
            }

            if (recv(socketVal, (char *)confirmation, sizeof(confirmation), 0) < 0)
            {
                fprintf(stderr, "Unable to recieve confirmation of sent message: %s\n", strerror(errno));
                return 1;
            }
            if (strcmp(confirmation, invalidUser) == 0)
            {
                printf("User is not available \n");
            }else if (strcmp(confirmation, yesValue) != 0)
            {
                fprintf(stderr, "Unable to recognize confirmation\n");
                return 1;
            }
        }else{
            close(socketVal);
            return 0;
        }
    }
}

void * handleMessages(void * val)
{
    int mStatus = 0;
    int sockVal = *(int*)val;

    char message[4096];
    char oBuffer[4096];
    char sBuffer[512];
    char tBuffer[10];

    memset(message, 0, sizeof(message));
    memset(oBuffer, 0, sizeof(oBuffer));
    memset(sBuffer, 0, sizeof(sBuffer));
    
    char *sender = sBuffer;
    char *type = tBuffer;
    char *output = oBuffer;

    while(1)
    {
        if (recv(sockVal, (char *)message, sizeof(message), 0) < 0)
        {
            fprintf(stderr, "Unable to recieve from server: %s\n", strerror(errno));
            exit(1);
        }

        if (message[0] != 'P' && message[0] != 'D')
        {    
            memset(message, 0, sizeof(message));
            memset(output, 0, sizeof(output));        
            memset(sender, 0, sizeof(sender));
            memset(type, 0, sizeof(type));
            continue;
        }
        type = strtok(message, ":");
        sender = strtok(NULL, ":");
        output = strtok(NULL, "\n");


        /* Create History Line */
        char added_line[4096];
        memset(added_line, 0, sizeof(added_line));
        
        if (strcmp(type, "P") == 0)
        {
            printf("\n***Public Message from %s***\n", sender);
            printf("%s\n", output);
            
            sprintf((char  *)added_line,"(Public) Sender%s: Message%s\n", sender, output);
            printf("Added line: %s\n", added_line);
            memset(added_line, 0, sizeof(added_line));
        }else{
            printf("\n***Direct Message from %s***\n", sender);
            printf("%s\n", output);
            
            sprintf((char  *)added_line,"(Private) Sender%s: Message%s\n", sender, output);
            printf("Added line: %s\n", added_line);
            memset(added_line, 0, sizeof(added_line));
        }



        printf(">>> ");
        fflush(stdout);
        memset(message, 0, sizeof(message));
        memset(output, 0, sizeof(output));        
        memset(sender, 0, sizeof(sender));
        memset(type, 0, sizeof(type));
    }
    
}
