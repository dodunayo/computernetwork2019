#include <arpa/inet.h>
#include <errno.h>
#include <ncurses.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <stdio.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>

#define WIDTH 43
#define HEIGHT 21
#define PADLX 1
#define PADRX WIDTH - 2

// Global variables recording the state of the game
// Position of ball
int ballX, ballY;
// Movement of ball
int dx, dy;
// Position of paddles
int padLY, padRY;
// Player scores
int scoreL, scoreR;

// ncurses window
WINDOW *win;

void *host(void *);
void *client(void *);

typedef struct {
    int ss;
    struct sockaddr_in sin;
} sock_sin;

typedef struct {
    int port;
    char * host;
} gameInfo;

/* Draw the current game state to the screen
 * ballX: X position of the ball
 * ballY: Y position of the ball
 * padLY: Y position of the left paddle
 * padRY: Y position of the right paddle
 * scoreL: Score of the left player
 * scoreR: Score of the right player
 */
void draw(int ballX, int ballY, int padLY, int padRY, int scoreL, int scoreR) {
    // Center line
    int y;
    for(y = 1; y < HEIGHT-1; y++) {
        mvwaddch(win, y, WIDTH / 2, ACS_VLINE);
    }
    // Score
    mvwprintw(win, 1, WIDTH / 2 - 3, "%2d", scoreL);
    mvwprintw(win, 1, WIDTH / 2 + 2, "%d", scoreR);
    // Ball
    mvwaddch(win, ballY, ballX, ACS_BLOCK);
    // Left paddle
    for(y = 1; y < HEIGHT - 1; y++) {
        int ch = (y >= padLY - 2 && y <= padLY + 2)? ACS_BLOCK : ' ';
        mvwaddch(win, y, PADLX, ch);
    }
    // Right paddle
    for(y = 1; y < HEIGHT - 1; y++) {
        int ch = (y >= padRY - 2 && y <= padRY + 2)? ACS_BLOCK : ' ';
        mvwaddch(win, y, PADRX, ch);
    }
    // Print the virtual window (win) to the screen
    wrefresh(win);
    // Finally erase ball for next time (allows ball to move before next refresh)
    mvwaddch(win, ballY, ballX, ' ');
}

/* Return ball and paddles to starting positions
 * Horizontal direction of the ball is randomized
 */
void reset() {
    ballX = WIDTH / 2;
    padLY = padRY = ballY = HEIGHT / 2;
    // dx is randomly either -1 or 1
    dx = (rand() % 2) * 2 - 1;
    dy = 0;
    // Draw to reset everything visually
    draw(ballX, ballY, padLY, padRY, scoreL, scoreR);
}

/* Display a message with a 3 second countdown
 * This method blocks for the duration of the countdown
 * message: The text to display during the countdown
 */
void countdown(const char *message) {
    int h = 4;
    int w = strlen(message) + 4;
    WINDOW *popup = newwin(h, w, (LINES - h) / 2, (COLS - w) / 2);
    box(popup, 0, 0);
    mvwprintw(popup, 1, 2, message);
    int countdown;
    for(countdown = 3; countdown > 0; countdown--) {
        mvwprintw(popup, 2, w / 2, "%d", countdown);
        wrefresh(popup);
        sleep(1);
    }
    wclear(popup);
    wrefresh(popup);
    delwin(popup);
    padLY = padRY = HEIGHT / 2; // Wipe out any input that accumulated during the delay
}

/* Perform periodic game functions:
 * 1. Move the ball
 * 2. Detect collisions
 * 3. Detect scored points and react accordingly
 * 4. Draw updated game state to the screen
 */
void tock() {
    // Move the ball
    ballX += dx;
    ballY += dy;

    // Check for paddle collisions
    // padY is y value of closest paddle to ball
    int padY = (ballX < WIDTH / 2) ? padLY : padRY;
    // colX is x value of ball for a paddle collision
    int colX = (ballX < WIDTH / 2) ? PADLX + 1 : PADRX - 1;
    if(ballX == colX && abs(ballY - padY) <= 2) {
        // Collision detected!
        dx *= -1;
        // Determine bounce angle
        if(ballY < padY) dy = -1;
        else if(ballY > padY) dy = 1;
        else dy = 0;
    }

    // Check for top/bottom boundary collisions
    if(ballY == 1) dy = 1;
    else if(ballY == HEIGHT - 2) dy = -1;

    // Score points
    if(ballX == 0) {
        scoreR = (scoreR + 1) % 100;
        reset();
        countdown("SCORE -->");
    } else if(ballX == WIDTH - 1) {
        scoreL = (scoreL + 1) % 100;
        reset();
        countdown("<-- SCORE");
    }
    // Finally, redraw the current state
    draw(ballX, ballY, padLY, padRY, scoreL, scoreR);
}

/* Listen to keyboard input
 * Updates global pad positions
 */
void *listenInput(void *args) {
    while(1) {
        switch(getch()) {
            case KEY_UP: padRY--;
             break;
            case KEY_DOWN: padRY++;
             break;
            case 'w': padLY--;
             break;
            case 's': padLY++;
             break;
            default: break;
       }
    }
    return NULL;
}

void initNcurses() {
    initscr();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);
    curs_set(0);
    refresh();
    win = newwin(HEIGHT, WIDTH, (LINES - HEIGHT) / 2, (COLS - WIDTH) / 2);
    box(win, 0, 0);
    mvwaddch(win, 0, WIDTH / 2, ACS_TTEE);
    mvwaddch(win, HEIGHT-1, WIDTH / 2, ACS_BTEE);
}

int main(int argc, char *argv[]) {
    FILE *logfile = fopen("log.txt", "a");
    // Process args
    // refresh is clock rate in microseconds
    // This corresponds to the movement speed of the ball
    int refresh;
    int PORT;
    bool  isHost;
    char *HOST;
    char difficulty[10];
    if(argc >=  3 && argc <= 4)
    {
        if(strcmp(argv[1], "--host")==0)
        {
            isHost = true;
            PORT = atoi(argv[2]);
            HOST = "NONE";
            printf("Please select the difficulty level (easy, medium or hard): ");
            scanf("%s", &difficulty);
            if(strcmp(difficulty, "easy") == 0) refresh = 80000;
                else if(strcmp(difficulty, "medium") == 0) refresh = 40000;
                else if(strcmp(difficulty, "hard") == 0) refresh = 20000; 
                else{
                    printf("ERROR: Unable to recognized difficulty!\n");
                    exit(1);
                }
        }else{
            HOST=argv[1];
            PORT=atoi(argv[2]);
        }
    }else{
        printf("Usage: \n./netpong [--host] [PORT] [DIFFICULTY]\n---or---\n./netpong HOSTNAME PORT\n");
        exit(0);
    }
    gameInfo gam  = {PORT, HOST};

    if(isHost)
    {
        printf("Current Host. Looking for opponents.\n");

        struct sockaddr_in sin, client_addr;
        char buffer[4096];
        int ss, addr_len;

        bzero((char  *)&sin, sizeof(sin));
        sin.sin_family = AF_INET;
        sin.sin_addr.s_addr = INADDR_ANY;
        sin.sin_port = htons(PORT);

        if ((ss = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
        {
            fprintf(logfile, "Error: Socket Failure.\n");
            exit(1);
        }

        if ((bind(ss, (struct sockaddr *)&sin, sizeof(sin))) <0)
        {
            fprintf(logfile, "Error: Bind Failure.\n");
            exit(1);
        }
        sock_sin temp2 = {ss, client_addr};
        while(1)
        {
            char response[4096];
            int rs;
            if((rs = recvfrom(ss, response, sizeof(response), 0, (struct sockaddr *)&client_addr, (socklen_t *)&addr_len)) != -1)
            {
                break;
            }
        }
        pthread_t hThread;
        pthread_create(&hThread, NULL, host, (void*)&temp2);
    }else
    {
        pthread_t cThread;
        pthread_create(&cThread, NULL, client, (void *)&gam);
    }
    
    // Set up ncurses environment
    initNcurses();

    // Set starting game state and display a countdown
    reset();
    countdown("Starting Game");

    // Listen to keyboard input in a background thread
    pthread_t pth;
    pthread_create(&pth, NULL, listenInput, NULL);

    countdown("Starting Game");

    // Main game loop executes tock() method every REFRESH microseconds
    struct timeval tv;
    while(1) {
        gettimeofday(&tv,NULL);
        unsigned long before = 1000000 * tv.tv_sec + tv.tv_usec;
        tock(); // Update game state
        gettimeofday(&tv,NULL);
        unsigned long after = 1000000 * tv.tv_sec + tv.tv_usec;
        unsigned long toSleep = refresh - (after - before);
        // toSleep can sometimes be > refresh, e.g. countdown() is called during tock()
        // In that case it's MUCH bigger because of overflow!
        if(toSleep > refresh) toSleep = refresh;
        usleep(toSleep); // Sleep exactly as much as is necessary
    }

    // Clean up
    pthread_join(pth, NULL);
    endwin();
    return 0;
}

void *hostSend(void *args) {
    FILE *logfile = fopen("log.txt", "a");
    struct sockaddr_in client_addr = ((sock_sin *)args)->sin;
    int ss = ((sock_sin *)args)->ss;
    int addr_len = sizeof(client_addr);
    while(1)
    {
        char buffer[4096];
        memset(buffer, 0, sizeof(buffer));
        sprintf(buffer, "%d", padLY);
        if (sendto(ss, buffer, strlen(buffer), 0, (struct sockaddr *)&client_addr, sizeof(struct sockaddr)) < 0)
        {
            fprintf(logfile, "Error: Imable to send current paddition position: %s\n", strerror(errno));
            exit(1);
        }
        usleep(1000);
    }
}

void *hostRecv(void *args)
{
    FILE *logfile = fopen("log.txt", "a");
    struct sockaddr_in client_addr = ((sock_sin *)args)->sin;
    int ss = ((sock_sin *)args)->ss;
    int addr_len = sizeof(client_addr);
    while (1)
    {
        char buffer[4096];
        memset(buffer, 0, sizeof(buffer));
        int rs;
        while((rs = recvfrom(ss, buffer, sizeof(buffer), 0, (struct sockaddr *)&client_addr, (socklen_t *)&addr_len)) < 0){}
        padRY = atoi(buffer);
    }
}

void *clientSend(void *args)
{
    FILE *logfile = fopen("log.txt", "a");
    struct sockaddr_in sin = ((sock_sin *)args)->sin;
    int ss = ((sock_sin *)args)->ss;
    int addr_len = sizeof(sin);
    while(1)
    {
        char buffer[4096];
        memset(buffer, 0, sizeof(buffer));
        sprintf(buffer, "%d", padRY);
        if (sendto(ss, buffer, strlen(buffer), 0, (struct sockaddr *)&sin, sizeof(struct sockaddr)) < 0)
        {
            fprintf(logfile, "Error: Imable to send current paddition position: %s\n", strerror(errno));
            exit(1);
        }
        usleep(1000);
    }
}

void *clientRecv(void *args)
{
    FILE *logfile = fopen("log.txt", "a");
    struct sockaddr_in sin = ((sock_sin *)args)->sin;
    int ss = ((sock_sin *)args)->ss;
    int addr_len = sizeof(sin);
    while (1)
    {
        char buffer[4096];
        memset(buffer, 0, sizeof(buffer));
        int rs;
        while((rs = recvfrom(ss, buffer, sizeof(buffer), 0, (struct sockaddr *)&sin, (socklen_t *)&addr_len)) < 0){}
        padLY = atoi(buffer);
    }
}

void *host(void *args)
{
    FILE *logfile = fopen("log.txt", "a");
    
    struct sockaddr_in client_addr = ((sock_sin *)args)->sin;
    int ss = ((sock_sin *)args)->ss;

    int addr_len = sizeof(client_addr);

    sock_sin z = {ss,  client_addr};

    pthread_t sendValue;
    pthread_create(&sendValue, NULL, hostSend, (void *)&z);

    pthread_t recvValue;
    pthread_create(&recvValue, NULL, hostRecv, (void *)&z);

}

void *client(void *args)
{
    FILE *logfile = fopen("log.txt", "a");

    int PORT = ((gameInfo *)args)->port;
    char *HOST = ((gameInfo *)args)->host;

    struct hostent *hp;
    struct sockaddr_in sin;
    int ss, len, addr_len;

    hp = gethostbyname(HOST);
    if (!hp)
    {
        fprintf(logfile, "Unknown Host\n");
        exit(1);
    }

    bzero((char*)&sin, sizeof(sin));
    sin.sin_family = AF_INET;
    bcopy(hp->h_addr, (char *)&sin.sin_addr, hp->h_length);
    sin.sin_port = htons(PORT);

    if ((ss = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        fprintf(logfile, "Error: Socket Failure\n");
        exit(1);
    }

    addr_len = sizeof(sin);

    char buffer[4096] = "ready\0";
    if (sendto(ss, buffer, strlen(buffer), 0, (struct sockaddr *)&sin, sizeof(struct sockaddr))  < 0)
    {
        fprintf(logfile, "Error. Unable to send Ready Messge: %s\n", strerror(errno));
        exit(1);
    }
    sock_sin z = {ss, sin};

    pthread_t sendValue;
    pthread_create(&sendValue, NULL, clientSend, (void *)&z);

    pthread_t recvValue;
    pthread_create(&recvValue, NULL, clientRecv, (void *)&z);
}
